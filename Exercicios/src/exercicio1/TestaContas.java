package exercicio1;

public class TestaContas {
	public static void main(String[] args){
		Conta umaConta = new Conta();
		Conta umaContaCorrente = new ContaCorrente();
		Conta umaContaPoupanca = new ContaPoupanca();
		
		umaConta.deposita(1000);
		umaContaCorrente.deposita(1000);
		umaContaPoupanca.deposita(1000);
		
		umaConta.atualiza(0.01);
		umaContaCorrente.atualiza(0.01);
		umaContaPoupanca.atualiza(0.01);
		
		System.out.println(umaConta.getSaldo());
		System.out.println(umaContaCorrente.getSaldo());
		System.out.println(umaContaPoupanca.getSaldo());
		
	}
	

}
